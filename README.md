# NExT Community of Practice: Nutzerzentriertes Design

Seit Sommer 2023 kommt die Community of Practice ›Nutzerzentriertes Design‹ vier Mal pro Jahr zusammen. 

Sie bietet Austausch für Designer:innen, User-Reseacher:innen und alle anderen mit Interesse an nutzerzentrierter Entwicklung von Produkten, Services und Normen.

Die Community wird geleitet von Maria Formisano von der Deutschen Rentenversicherung Bund und Martin Jordan vom DigitalService des Bundes. 

## Aufgaben der Community
Die Community entstand aus dem identifizierten Bedürnis nach einer Austauschplattform für alle im öffentlichen Sektor, die mit Fokus auf nutzerzentriertes Design arbeiten. 

Sie soll einen geschützten Raum geben, um:

* über unsere Arbeit zu sprechen
* Erfahrungen auszutauschen
* miteinander zu lernen
* einander zu unterstützen
* Dinge gemeinsam zu bearbeiten


## Über die Community 

Verwaltung erkennt zunehmend die Relevanz und Nützlichkeit von nutzerzentriertem Design beim Gestalten ihrer Leistungen und internen Prozesse. Nachdem Politik und Leitungsebene der Verwaltung seit mehreren Legislaturperioden zum Ziel erklärt hatten, dass angebotene Leistungen an den Bedürfnissen von Bürger:innen und Nutzenden ausgerichtet sein müssen, folgen operative Taten.

Seit recht kurzer Zeit gibt es nun ausgebildete Expert:innen mit Hintergrund in nutzerzentriertem Design und Nutzerforschung in einer wachsenden Anzahl von Organisationen der öffentlichen Hand. Zudem gibt es zahlreiche Verwaltungsmitarbeitende, die regelmäßig auf nutzerzentrierte Designansätze wie Design Thinking in ihrer Arbeit setzen. 

Unsere Community soll eine Austauschplattform für alle, die Berührungspunkte mit Design und Nutzerforschung haben, sein. In regelmäßigen Community-Treffen sollen Designer:innen, Nutzerforscher:innen und andere, die an deren Denken und Machen interessiert sind, zusammenkommen können, um einander ihre Arbeit und Arbeitsweisen vorzustellen, sich miteinander über ihre Erfahrungen auszutauschen und voneinander zu lernen.