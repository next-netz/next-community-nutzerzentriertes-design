# Bisherige Community-Treffen

## Treffen Nr. 6 — November 2024: ›Holistisches Servicedesign‹
Servicedesign ist die koordinierte Gestaltung von Leistungen entlang der Bedürfnisse von Nutzenden. Im Verwaltungskontext und öffentlichen Sektor heißt das, orientiert an Lebenslagen, oft über verschiedene Einheiten und Behörden zerstreute Angebote zu bündeln und dabei fundamental zu vereinfachen. Das schafft erhöhte Zugänglichkeit für Menschen und entlastet die öffentlichen Organisationen.

— mit inhaltlichen Beiträgen vom Digital First des Amt für IT und Digitalisierung der Freien Hansestadt Hamburg und vom DigitalService des Bundes


## Treffen Nr. 5 — September 2024: ›Gute Formulare gestalten‹
Formulare sind der wichtigste Zugang zu Verwaltungsservices. Formulare waren, sind und werden auch mit zunehmender Automatisierung und einer proaktiven Verwaltung der meistgenutzte Berührungspunkt von Bürger:innen mit dem Staat sein. Damit sie funktionieren und fehlerfrei ausgefüllt werden können, müssen sie verständlich und nachvollziehbar strukturiert sein. Egal, ob auf Papier oder online.

Wir besprachen, wie die Arbeit an guten Formularen konkret aussieht. Wir hörten von Kolleg:innen aus verschiedenen Teilen der öffentlichen Hand. Sie berichteten, wie sie an neuen Formularen arbeiten und alte umstrukturieren – und wie Nutzende möglichst früh in den Entwicklungsprozess eingebunden werden.

— mit inhaltlichen Beiträgen von der Stadt Köln und vom DigitalService des Bundes


## Treffen Nr. 4 — Juni 2024: ›Digitale Barrierefreiheit‹
Gut gestaltete digitale staatliche Angebote können ein neues Maß an Zugang für Bürger:innen bieten. Menschen mit Behinderungen können plötzlich selbstständig und ohne fremde Hilfe online Anträge stellen, wo nötig Formulare allein ausfüllen und eigenständig mit Institutionen interagieren. Digitale Barrierefreiheit bedeutet für viele Menschen Selbstermächtigung. Doch noch sind nicht alle Angebote des Staates vollkommen barrierefrei. Bei unserem Treffen sprachen wir über den Weg dahin und hörten von mehreren Beitragenden, wie sie praktisch Barrieren beseitigen und Zugang schaffen.

— mit inhaltlichen Beiträgen von der Deutschen Rentenversicherung Bund, der Überwachungsstelle des Bundes für Barrierefreiheit von Informationstechnik und vom DigitalService des Bundes


## Treffen Nr. 3 — Februar 2024: ›Nutzerzentrierung durch Nutzereinbindung‹
Der Koalitionsvertrag verspricht Nutzerzentrierung seit 2021, der Servicestandard beschreibt sie seit 2020 und Bundeskanzlerin Angelika Merkel wünschte sie sich schon 2015. An politischen Forderungen, Wünschen oder Lippenbekenntnissen fehlt es nicht, an operativer Umsetzung jedoch sehr. Noch immer entstehen zahllose digitale Angebote des Staats ohne jegliche Einbindung ihrer späteren Nutzenden. Die Folge sind Produkte, Informationsseiten und Services, die nicht verständlich sind, gut bedienbar sind oder überhaupt nur nützlich sind.
 
Es geht jedoch auch anders. Eine wachsende Anzahl von Teams im öffentlichen Sektor bindet früh und oft Nutzende in den Entwicklungsprozess ein. Diesen wollten wir Raum geben. In diesem Treffen wollten wir aufzeigen und diskutieren, wie gute Praxis der Nutzerforschung, Ko-Kreation und iterative Designentwicklung praktisch aussehen und welche Rahmenbedingungen sie benötigen.

— mit inhatlichen Beiträgen von der Deutschen Rentenversicherung Bund, Dataport und vom DigitalService des Bundes


## Treffen Nr. 2 — Dezember 2023: ›Klare Sprache, verständliche Verwaltung‹
In Behörden und öffentlichen Einrichtungen ist eine menschenfreundliche und klare Sprache von entscheidender Bedeutung – damit Informationen erfasst werden, Verständigung gelingt und Interaktionen erfolgreich sind. Doch bislang sind einfache Sprache und verständliche Verwaltungskommunikation selten.

Barrierefreiheit, Vertrauen in Prozesse, wahrgenommene Transparenz, messbare Effizienz und empfundene Bürgerzufriedenheit aber hängen davon ab. Langsam investieren Organisationen der öffentlichen Hand in Content-Design, UX-Writing und bedürfnisorientiertes Texten. Das macht nicht nur neue digitale Angebote besser. Verwaltung, die an die Sprache und Denkmodelle von Bürger*innen anknüpft, entlastet Ämter und stärkt die Beziehung zum Staat.

— mit inhatlichen Beiträgen von der Landeshauptstadt München und vom DigitalService des Bundes


## Treffen Nr. 1 — September 2023: ›Design-Systeme und Komponentenbibliotheken‹  

Staatliche Leistungen müssen weiterhin zu hunderten in digitale Services verwandelt werden. Diese müssen für ihre Nutzer*innen nützlich, nutzbar und zugänglich sein. Um das effektiv und effizient zu erreichen, benötigen wir robuste, getestete wiederverwendbare Stile, Komponenten und Muster, auf die wir als Bausteine zurückgreifen können. Daher spielen Design-Systeme und Komponentenbibliotheken sowie zugrundeliegende Prinzipien eine zunehmend große Rolle in Organisationen der öffentlichen Hand.

In einem zweieinhalbstündigen Treffen wollten wir Ist-Stände beschreiben, Hürden und Möglichkeiten verstehen und gemeinsame nächste Schritte beschreiben. Wir hörten von einigen Akteur:innen in kurzen Inputs, sahen Demos, diskutieren Fragen und sammeln Ideen.

— mit inhatlichen Beiträgen von der Deutschen Rentenversicherung Bund, Dataport und von Berlin Online


## Treffen Nr. 0 — Juli 2023: ›What’s NExT Nutzerzentriertes Design‹

Verwaltung erkennt zunehmend die Relevanz und Nützlichkeit von nutzerzentriertem Design beim Gestalten ihrer Leistungen und internen Prozesse. Nachdem Politik und Leitungsebene der Verwaltung seit mehreren Legislaturperioden zum Ziel erklärt hatten, dass angebotene Leistungen an den Bedürfnissen von Bürger:innen und Nutzenden ausgerichtet sein müssen, folgen operative Taten. Seit recht kurzer Zeit gibt es nun ausgebildete Expert*innen mit Hintergrund in nutzerzentriertem Design und Nutzerforschung in einer wachsenden Anzahl von Organisationen der öffentlichen Hand. Zudem gibt es zahlreiche Verwaltungsmitarbeitende, die regelmäßig auf nutzerzentrierte Designansätze wie Design Thinking in ihrer Arbeit setzen.  

— mit inhatlichen Beiträgen von der Bundesdruckerei, Dataport, ITZBund und vom DigitalService des Bundes

